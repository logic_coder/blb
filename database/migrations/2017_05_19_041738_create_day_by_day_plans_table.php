<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayByDayPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('day_by_day_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('travel_plan_date_id')->unsigned();
            $table->string('start_time');
            $table->string('stay_time');
            $table->string('place');
            $table->string('travel_time');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('travel_plan_date_id')->references('id')->on('travel_plan_dates')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('day_by_day_plans');
    }
}
