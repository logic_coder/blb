<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelPlanFinalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_plan_finals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('travel_plan_id')->unsigned()->comment('This Travel Plan id for final plan');
            $table->integer('travel_plan_group_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('travel_plan_id')->references('id')->on('travel_plans')->delete('cascade');
            $table->foreign('travel_plan_group_id')->references('id')->on('travel_plan_groups')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_plan_finals');
    }
}
