<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelPlanDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_plan_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('travel_plan_id')->unsigned();
            $table->date('date');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('travel_plan_id')->references('id')->on('travel_plans')->delete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_plan_dates');
    }
}
