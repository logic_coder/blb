<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userObj = new User();
        $userObj->name = 'admin';
        $userObj->email = 'admin@blb.com';
        $userObj->password = '$2y$10$wtUlc2Rx5j3z8/e5moNOVu/gDjNxCtTvXchUD3d4fY9KF3E9PSlAC'; // pass = 123456
        $userObj->save();
    }
}
