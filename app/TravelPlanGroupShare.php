<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelPlanGroupShare extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'travel_plan_group_shares';
    protected $fillable = ['travel_plan_group_id','travel_plan_id','user_id'];
}
