<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TravelPlanDate extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'travel_plan_dates';
    protected $fillable = ['travel_plan_id','date'];

    public function dayByDayPlan()
    {
        return $this->hasMany('App\DayByDayPlan');
    }
}
