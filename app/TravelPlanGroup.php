<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelPlanGroup extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'travel_plan_groups';
    protected $fillable = ['user_id', 'travel_plan_id', 'name'];

    public function sharedPlan()
    {
        return $this->hasMany('App\TravelPlanGroupShare');
    }
}
