<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TravelPlanFinal extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'travel_plan_finals';
    protected $fillable = ['travel_plan_id', 'travel_plan_group_id'];
}
