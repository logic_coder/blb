<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelPlan extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='travel_plans';
    protected $fillable = ['user_id','name'];

    public function planDate()
    {
        return $this->hasMany('App\TravelPlanDate');
    }
    public function travelGroup()
    {
        return $this->belongsTo('App\TravelPlanGroup');
    }
}
