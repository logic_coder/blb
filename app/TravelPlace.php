<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelPlace extends Model
{
    protected $table = 'travel_places';
    protected $fillable = ['country_id', 'city', 'types', 'latitude', 'longitude', 'opening_hour','closing_hour', 'holidays'];
}