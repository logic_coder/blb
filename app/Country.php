<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "countries";
    protected $fillable = ['name','code'];

    public function travelPlaces()
    {
        return $this->hasMany('App\TravelPlace');
    }
}
