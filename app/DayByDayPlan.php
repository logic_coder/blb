<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DayByDayPlan extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'day_by_day_plans';
    protected $fillable = ['travel_plan_date_id', 'start_time', 'stay_time','place','travel_time'];
}
