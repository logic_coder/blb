<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DayByDayPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ){
            case 'POST':
                return [
                    'start_time' => 'required',
                    'stay_time' => 'required',
                    'place' => 'required',
                ];
                break;

            case 'PUT':
                return [
                    'start_time' => 'required',
                    'stay_time' => 'required',
                    'place' => 'required',
                ];
                break;
        }
    }
}
