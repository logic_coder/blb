<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = [ 'Bandarban', 'Cox\'s Bazar', 'Dhaka', 'Rangamati', 'Sundarban' ];
        return view('home', compact('locations'));
    }

    public function citynameToLatLong($cityname) {

        $cityname = "Coxbazar";

        // http://maps.googleapis.com/maps/api/geocode/json?address=London
        $url        = "http://maps.googleapis.com/maps/api/geocode/json?address=".$cityname; 
        $content    = @file_get_contents($url);
        $jsondata   = json_decode($content,true);
//dd( $jsondata);
        $latitude = $longitude = $placeId = null;

        if( $jsondata ) {

            $latitude   = $jsondata['results'][0]['geometry']['location']['lat'];
            $longitude  = $jsondata['results'][0]['geometry']['location']['lng'];
            $placeId    = $jsondata['results'][0]['place_id'];
//            var_dump($latitude, $longitude, $placeId); die();

        }

        $place_detail_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeId.'&key=AIzaSyDlawkkDtL3fawiJWpOOx5phmJD09elRuo';
        $place_detail_content    = @file_get_contents($place_detail_url);
        $place_detail_jsondata   = json_decode($place_detail_content,true);
        dd( $place_detail_jsondata );
        $place_detail_jsondata  = json_decode($place_detail_content,true);
//        dd( $place_detail_jsondata);

        if( $place_detail_jsondata ) {
            $CountryDetails = $cityDetails = $streetAddress = $cityName = $countryName = $location = null;
            $travel_place_data['city'] = $place_detail_jsondata['result']['name'] ? $place_detail_jsondata['result']['name'] : '';
            $travel_place_data['types'] = $place_detail_jsondata['result']['types'][0] ? $place_detail_jsondata['result']['types'][0] : '';
            $travel_place_data['latitude'] = $latitude;
            $travel_place_data['longitude'] = $longitude;
            foreach ( $place_detail_jsondata['result']['address_components'] as $address ){
                if( in_array("country", $address["types"] ) ) {
                    $country_name = $address["long_name"];
                    $country = Country::where('name', '=' , $country_name)->first();
                    $country->travelPlaces()->create( $travel_place_data );
                    break;
                }else{
                    $country = Country::where('id', 23)->first();
                    $country->travelPlaces()->create( $travel_place_data );
                    break;
                }
            }
        }
    }

    public function searchForm(){
        $locations = [ 'Bandarban', 'Cox\'s Bazar', 'Dhaka', 'Rangamati', 'Sundarban' ];
        return view('home.search-form', compact('locations'));
    }
}
