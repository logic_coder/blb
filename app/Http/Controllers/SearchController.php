<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $avg_day_hour = "14:00:00";
    protected  $default_food = [

        'breakfast_start'   => '08:00:00',
        'breakfast_end'     => '09:00:00',
        'lunch_start'       => '13:00:00',
        'lunch_end'         => '14:00:00',
        'dinner_start'      => '21:00:00',
        'dineer_end'        => '22:00:00',

    ];

    protected $default_day = [
        'day_start' => '08:00:00',
        'day_end'  => '24:00:00'
    ];
    public function index(Request $request)
    {
        $places = [];

        $placeInfo = [
            'id' => 0,
            'name' => 'Laboni Sea Beach',
            'type' => [
                'Entertainment', 'Natural', 'Sports'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '22:00:00',
            'holiday' => [
                'Friday'
            ],
            'avg_stay_time' => '2 hours',
            'avg_cost' => '100',
            'status' => 0
        ];

        $places[] = $placeInfo;

        #New place

        $placeInfo = [
            'id' => 1,
            'name' => 'Paraclyding',
            'type' => [
                'Adventure', 'Natural'
            ],
            'opening_hour' => '12:00:00',
            'closing_hour' => '17:00:00',
            'holiday' => [
                'Friday', 'Saturday'
            ],
            'avg_stay_time' => '30 minutes',
            'avg_cost' => '2000',
            'status' => 0
        ];

        $places[] = $placeInfo;

        #New place
        $placeInfo = [
            'id' => 2,
            'name' => 'Handi',
            'type' => [
                'Entertainment', 'Cuisine'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '22:00:00',
            'holiday' => [
            ],
            'avg_stay_time' => '1 hour',
            'avg_cost' => '300',
            'status' => 0
        ];

        $places[] = $placeInfo;

        #New place
        $placeInfo = [
            'id' => 3,
            'name' => 'Radhuni',
            'type' => [
                'Entertainment', 'Cuisine'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '22:00:00',
            'holiday' => [
            ],
            'avg_stay_time' => '1 hour',
            'avg_cost' => '300',
            'status' => 0
        ];

        $places[] = $placeInfo;

        #New place
        $placeInfo = [
            'id' => 4,
            'name' => 'Jhaubon',
            'type' => [
                'Entertainment', 'Cuisine'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '22:00:00',
            'holiday' => [
            ],
            'avg_stay_time' => '1 hour',
            'avg_cost' => '300',
            'status' => 0
        ];

        $places[] = $placeInfo;

        #New place
        $placeInfo = [
            'id' => 5,
            'name' => 'Buemese Market',
            'type' => [
                'Entertainment', 'Shopping'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '22:00:00',
            'holiday' => [
                'Friday'
            ],
            'avg_stay_time' => '3 hours',
            'avg_cost' => '2000',
            'status' => 0
        ];

        $places[] = $placeInfo;

        #New place
        $placeInfo = [
            'id' => 6,
            'name' => 'Himchori',
            'type' => [
                'Adventure', 'Natural'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '17:00:00',
            'holiday' => [
                'Friday'
            ],
            'avg_stay_time' => '2 hours',
            'avg_cost' => '500',
            'status' => 0
        ];

        #New place
        $placeInfo = [
            'id' => 6,
            'name' => 'Inani Beach',
            'type' => [
                'Entertainment', 'Natural'
            ],
            'opening_hour' => '08:00:00',
            'closing_hour' => '17:00:00',
            'holiday' => [
                'Friday'
            ],
            'avg_stay_time' => '2 hours',
            'avg_cost' => '500',
            'status' => 0
        ];

        $places[] = $placeInfo;

        $user_criteria = [
            'start_date' => $request->get('start_date'),
            'end_date'   => $request->get('end_date'),
            'pace'       => $request->get('pace'),
            'budget'     => $request->get('budget')
        ];

        $user_preferances = [
            'Entertainment'  => '9',
            'Shopping'      => '7',
            'Cuisine'       => '6',
            'Adventure'     => '8'
        ];

        arsort($user_preferances);

        $date_diff = strtotime($user_criteria['end_date']) - strtotime($user_criteria['start_date']);
        $total_days = floor($date_diff / (60 * 60 * 24));

        $hour_per_day = floor((date('h', strtotime($this->avg_day_hour)) * $user_criteria['pace'] * 10)/100);
        $total_hours = $hour_per_day * $total_days;

        $plan = [];
//        $slot = 0;



        for ($day = 0; $day <= $total_days; $day++) {
            foreach ($places as $place) {
                $places[$place['id']]['day'.$day] = 0;
            }
//            echo '<pre>';print_r($places);die;
//            echo $total_days;
            $slot = 0;
            $plan[$day] = [];
            $total_spend_hour = '00:00:00';

            if($slot == 0) {
                $cuisine = $this->get_location_by_type($places, 'Cuisine');
                $end_time = strtotime('+'.$cuisine[0]['avg_stay_time'], strtotime($this->default_day['day_start']));
                if(!$cuisine[0]['day'.$day]){

                    $plan[$day][$slot] = [
                        'start_time' => $this->default_day['day_start'],
                        'end_time' => date('h:i:s', $end_time),
                        'place_id' => $cuisine[0]['id'],
                        'place' => $cuisine[0]['name']
                    ];
                }

                $places = $this->set_status_location($places, $cuisine[0]['id'], $day);
                $total_spend_hour =  date('h:i:s', strtotime($total_spend_hour) + strtotime($this->get_time_difference($this->default_day['day_start'], date('h:i:s', $end_time))));

                $slot++;
            }



            foreach ($user_preferances as $preferance=>$value){
                $preffered_places = $this->get_location_by_type($places, $preferance);
//                echo '<pre>';print_r($preffered_places);die;

                foreach ($preffered_places as $pp){
                    $start_time = date('h:i:s', $end_time);
                    $end_time = strtotime('+'.$pp['avg_stay_time'], strtotime($start_time));
                    if(!$pp['day'.$day]){
                        $plan[$day][$slot] = [
                            'start_time' => $start_time,
                            'end_time' => date('h:i:s', $end_time),
                            'place_id' => $pp['id'],
                            'place' => $pp['name']
                        ];
                    }
                    $places = $this->set_status_location($places, $pp['id'], $day);
                    $total_spend_hour =  date('h:i:s', strtotime($total_spend_hour) + strtotime($this->get_time_difference($start_time, date('h:i:s', $end_time))));

                    $slot++;
                }
            }

        // echo '<pre>';print_r($plan);die;    
        // echo $total_hours;die;

        return view('search.result', compact('places', 'plan'));
        }
    }


    public function get_location_by_type($places, $type){
        $result_places = array();
        foreach ($places as $place){
            if(in_array($type, $place['type'])){
                $result_places[] = $place;
            }
        }
        return $result_places;
    }


    public function set_status_location($places, $place_id, $day){
        $status = 1;
        foreach ($places as $place){
            if($place['id'] == $place_id){
                $places[$place['id']]['day'.$day] = $status;
            }
        }
        return $places;
    }

    public function get_time_difference($start_time, $end_time){
        $time1 = strtotime($start_time);
        $time2 = strtotime($end_time);
        return date('h:i:s', ($time2 - $time1));
    }


    // function for multidimentional in_array
    public function in_array_multi($val, $arr) {
        $found = false;
        foreach ($arr as $item) {
            if ($item === $arr) {
                $found = true;
                break;
            }
            elseif (is_array($item)) {
                $found = $this->in_array_multi($val, $item);
                if($found) {
                    break;
                }
            }
        }
        return $found;
    }

    public function check_array_color($places){
        $flag = 0;
        $i = 0;
        $total_places = count($places);
        while($i <= $total_places){
            if($places[$i]['status'] == $places[$i + 1]['status']){
                $i++;
                continue;
            }
            else{
                $flag = 1;
                break;
            }
        }
        return $flag;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo 'rgae'; die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //
}
