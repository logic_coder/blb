@extends('layouts.master')

@section('content')

    <div class="main-inner">
        <div class="content">
            <div class="mt-150">
                <div class="hero-image">
                    <div class="hero-image-inner" style="background-image: url('images/tmp/slider-large-3.jpg');">
                        <div class="hero-image-content">
                            <div class="container">
                            </div><!-- /.container -->
                        </div><!-- /.hero-image-content -->

                        <div class="hero-image-form-wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        {{ Form::open(['url'=>'search','id'=>"form-search", "name"=>"form-search","class"=>"p-t-15","role"=>"form","method"=>"post"]) }}
                                        <h2>Make your tour plan within 5 minutes.</h2>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-default required">
                                                    {{ Form::label('destination', 'Monthly Rent') }}
                                                    {{ Form::text('monthly_rent',null, ['class' => 'form-control', 'required']) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group col-md-4">
                                                    <label for="place">Start Date</label>
                                                    {{ Form::date('end_date', 'End Date', ['class' => 'form-control datepicker', 'required']) }}
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="place">End Date</label>
                                                    {{ Form::date('end_date', 'End Date', ['class' => 'form-control datepicker', 'required']) }}
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="persons">Persons</label>
                                                    <input type="persons" class="form-control" id="place" placeholder="persons">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block">Search</button>
                                        {{ Form::close() }}
                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->
                                <div class="clearfix"> </div>


                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6"></div>
                                </div><!-- /.row -->




                            </div><!-- /.container -->
                        </div><!-- /.hero-image-form-wrapper -->
                    </div><!-- /.hero-image-inner -->
                </div><!-- /.hero-image -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.content -->
@endsection
