	<!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
	<script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
	<script src="<?= asset('app/lib/angular/angular-route.js') ?>"></script>

	<!-- AngularJS Application Scripts -->
	<script src="<?= asset('app/app.js') ?>"></script>

	<!-- User Take survey -->

	@include('angularfile.userpanel.userpanelController')  
	@include('angularfile.userpanel.userpanelDirective') 
