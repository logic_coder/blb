<script>
	
	angular.module('records')

    .directive('takemySurvey', function () {
        return {
            restrict: 'EA',
            scope: {      
                previewData: '=',
                saveSurveyans: '&'
            },
            templateUrl: '<?php echo asset('app/directive/userpanel/takeSurvey.blade.php') ?>',
            
            link: function (scope, iElement, iAttrs) {
                
                var i = 1;
                var qkey = 0;
                var headerTitle         = null;
                var headerDescription   = null;
                scope.lastQuestion      = 0;
                scope.defaultProgress   = 0;
                scope.question = [];


                // Finally submit survey
                
                scope.submitSurvey = function(surveyId, qvalue) {

                    swal({   
                        title: "Are you sure you want to submit your survey?",  
                        type: "warning",  
                        showCancelButton: true,   
                        closeOnConfirm: false,   
                        showLoaderOnConfirm: true,
                        confirmButtonColor: '#88A755',

                    }, function() {   
                        //console.log('Directive data:', qvalue);
                        qvalue.finalSubmit = 1;
                        scope.saveSurveyans( {  surveyId: surveyId, data: qvalue } );
                        
                    });

                }


            }
        };
    })
    

</script>