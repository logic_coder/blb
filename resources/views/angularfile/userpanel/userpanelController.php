<script>

	angular.module('records')

	.controller('userpanelCtrl', ['$scope', '$http', 'API_URL', function ($scope, $http, API_URL) {

		$scope.saveSurveyAns = function(surveyId, data) {

			data.mongoid 		= surveyId;
			data.surveyid 		= angular.element('#surveyid').val();
			data.trespondant 	= angular.element('#trespondant').val();
			data.latitude 		= angular.element('#latitude').val();
			data.longitude 		= angular.element('#longitude').val();
			data.points 		= angular.element('#points').val();
			// data.previousRankOptions = angular.element('#preValOpt').val();
			data.previousRankOptions = angular.element('#preValOptId' + data.id).val();

			var message 		= angular.element('#message').val();

			var postUrl = API_URL + 'user/answer/save';
			// console.log('Data:', data);

			$http
			.post(postUrl, data)
			.then(function(res) {
				console.log('Controller Response:', res.data);
				if( res.data == '9' ) {

					swal({
						title: "This survey is no longer available!",
						imageUrl: "<?= asset('images/sorry.jpg') ?>",
						timer: 3000
					});
                    
                } else if(res.data == '8' ) {

                	swal({
						title: "You have already taken this survey!",
						imageUrl: "<?= asset('images/sorry.jpg') ?>",
						timer: 3000
					});
					
                } else if(res.data == '6' ) {

                    swal({
						title: "Your survey answer is submited! Please complete your profile.",
						imageUrl: "<?= asset('images/smile.jpg') ?>",
						timer: 3000
					});
              
                } else if(res.data == '200' ) {

                    swal({
						title: "Your survey answer is submited!",
						imageUrl: "<?= asset('images/smile.jpg') ?>",
						timer: 3000
					});
              
                } else if(res.data == '404' ) {

                    swal({
						title: "We can not process your submited data.",
						imageUrl: "<?= asset('images/sorry.jpg') ?>",
						timer: 9000
					});
              
                } else {
                    
                    // Stop showing success message for every time question saving 
                }
				
			}, function(err) {
				console.log('Controller Error:', err);
				swal({
					title: "A problem has been occurred during data saving!",
					imageUrl: "<?= asset('images/sorry.jpg') ?>" ,
					timer:2000
					
				});
				
			})
			
		}


	}]);


 
</script>	