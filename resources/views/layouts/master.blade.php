<!DOCTYPE html>
<html ng-app="records">
<head>
    <title>Bhoboghuray</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    
    <link href="http://fonts.googleapis.com/css?family=Nunito:300,400,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{!! asset('css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/owl.carousel.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/colorbox.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-select.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/fileinput.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/superlist.css') !!}">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
    
        <!-- Custom  -->

    <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">

    <!-- Angular  -->
    <link rel="stylesheet" href="{!! asset('css/angular/angular-material.min.css') !!}">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=PT+Sans:400,700'>


    <link rel="stylesheet" type="text/css" href="http://api.tiles.mapbox.com/mapbox.js/v2.2.2/mapbox.css" />
    <title data-format="">
        Personalize your trip | Utrip   </title>

    <link rel="shortcut icon" href="#" type="image/x-icon">
    
    <script src="{!! asset('assets/v2/vendor/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script>
        
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../assets/v2/vendor/jquery/jquery-2.1.1.min.js"><\/script>')</script>
    

<script>
            var $Config = {};
    </script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;client=gme-utripinc"></script>

<script src="{!! asset('assets/v2/js/build/vendor.min2e8e.js?rel=1493769838734') !!}"></script>

<script src="{!! asset('assets/v2/vendor/richmarker/richmarker.js') !!}"></script>

<script src="{!! asset('assets/v2/js/build/utrip.min2e8e.js?rel=1493769838734') !!}"></script>

<script type="text/javascript" src="http://api.tiles.mapbox.com/mapbox.js/v2.2.2/mapbox.js"></script>
<script type="text/javascript" src="{!! asset('assets/v2/vendor/leaflet/leaflet-heat.js') !!}"></script>
<script type="text/javascript">
/*<![CDATA[*/
L.mapbox.accessToken = 'pk.eyJ1IjoidXRyaXAiLCJhIjoiY2lmNGRwcDA1MDI3MnJya2xmc213d2YxdSJ9.w2aXXRBoOuYXuBF1tCJImQ';
/*]]>*/
</script>


    <!-- Angular Dependency -->
    @include('angularfile.angularDependency') 

</head>


<body class="responsive v2 inner personalize">

<div class="page-wrapper">
    
    <header class="header header-transparent">
        <div class="header-wrapper">
            <div class="container">
                <div class="header-inner">
                    <div class="header-logo">
                        <a href="{{url('/home')}}">
                            <img src="images/logo-white.png" alt="Logo">
                            <span>Bhoboghuray</span>
                        </a>
                    </div><!-- /.header-logo -->


                    <!-- START top menu-->
                        @include('partials/top_menu')
                    <!-- END top menu-->


                </div><!-- /.header-inner -->
            </div><!-- /.container -->
        </div><!-- /.header-wrapper -->
    </header><!-- /.header -->
    <div class="main">
        <div class="main-inner">
            <div class="content">
                @yield('content')
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

    @include('partials/footer')

</div><!-- /.page-wrapper -->
<!-- BEGIN VENDOR JS -->
<script src="js/map.js" type="text/javascript"></script>

<script src="js/collapse.js" type="text/javascript"></script>
<script src="js/carousel.js" type="text/javascript"></script>
<script src="js/transition.js" type="text/javascript"></script>
<script src="js/dropdown.js" type="text/javascript"></script>
<script src="js/tooltip.js" type="text/javascript"></script>
<script src="js/tab.js" type="text/javascript"></script>
<script src="js/alert.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="js/jquery.colorbox-min.js" type="text/javascript"></script>
<script src="js/jquery.flot.min.js" type="text/javascript"></script>
<script src="js/jquery.flot.spline.js" type="text/javascript"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/infobox.js"></script>
<script type="text/javascript" src="js/markerclusterer.js"></script>
<script type="text/javascript" src="js/jquery-google-map.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/fileinput.min.js"></script>
<script src="js/superlist.js" type="text/javascript"></script>
  
@yield('plugin-script')
<!-- BEGIN PAGE LEVEL JS -->
@yield('page-script')
<!-- END PAGE LEVEL JS -->

</body>
</html>
