@extends('layouts.master')

@section('content')


<div class="fullscreen-wrapper">
    <div class="fullscreen-scroll">
        <div class="fullscreen-scroll-inner">
            <div class="fullscreen-scroll-padding col-sm-8">
                <form class="filter" method="post" action="http://preview.byaviators.com/template/superlist/listing-map.html?">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <input type="text" placeholder="Keyword" class="form-control">
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->

                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <select class="form-control" title="Select Location">
                                    <option>Bronx</option>
                                    <option>Brooklyn</option>
                                    <option>Manhattan</option>
                                    <option>Staten Island</option>
                                    <option>Queens</option>
                                </select>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->

                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <select class="form-control" title="Select Category">
                                    <option value="">Automotive</option>
                                    <option value="">Jobs</option>
                                    <option value="">Nightlife</option>
                                    <option value="">Services</option>
                                </select>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->

                    <hr>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="filter-actions">
                                <a href="#"><i class="fa fa-close"></i> Reset Filter</a>
                                <a href="#"><i class="fa fa-save"></i> Save Search</a>
                            </div><!-- /.filter-actions -->
                        </div><!-- /.col-* -->

                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary">Redefine Search Result</button>
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->
                </form>


                <h2 class="page-title">
                    1802 results matching your query

                    <form method="get" action="http://preview.byaviators.com/template/superlist/listing-map.html?" class="filter-sort">
                        <div class="form-group">
                            <select title="Sort by">
                                <option name="price">Price</option>
                                <option name="rating">Rating</option>
                                <option name="title">Title</option>
                            </select>
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <select title="Order">
                                <option name="ASC">Asc</option>
                                <option name="DESC">Desc</option>
                            </select>
                        </div><!-- /.form-group -->
                    </form>
                </h2><!-- /.page-title -->

                <div class="cards-row">
                    @foreach($plan as $day)
                        @foreach($day as $slot)
                            <?php
                                echo '<pre>';print_r($slot);
                                ?>
                        <div class="card-row">
                            <div class="card-row-inner">
                                <div class="card-row-image" data-background-image="images/tmp/product-1.jpg">
                                    {{--<div class="card-row-label"><a href="#">{{ $places[$slot['place_id']]['type'] }}</a></div><!-- /.card-row-label -->--}}

{{--                                    <div class="card-row-price">${{ $places[$slot['place_id']]['avg_cost'] }} / person</div><!-- -->--}}

                                </div><!-- /.card-row-image -->

                                <div class="card-row-body">
{{--                                    <h2 class="card-row-title"><a href="#">{{ $places[$slot['place_id']]['name'] }}</a></h2>--}}
                                    <div class="card-row-content"><p>And why did 'I' have to take a cab? Bender, quit destroying the universe! I've been there. My folks were always on me to groom myself and...</p></div><!-- /.card-row-content -->
                                </div><!-- /.card-row-body -->

                                <div class="card-row-properties">
                                    <dl>

                                        {{--<dd>Price</dd><dt>${{ $places[$slot['place_id']]['avg_cost'] }} / person</dt>--}}



                                        {{--<dd>Category</dd><dt>{{ $places[$slot['place_id']]['type'] }}</dt>--}}



                                        <dd>Location</dd><dt>Cox's Bazar</dt>



                                    </dl>
                                </div><!-- /.card-row-properties -->
                            </div><!-- /.card-row-inner -->
                        </div><!-- /.card-row -->

                        @endforeach
                    @endforeach

                </div><!-- /.cards-row -->

            </div><!-- /.fullscreen-scroll-padding -->
        </div><!-- /.fullscreen-scroll-inner -->
    </div><!-- /.fullscreen-scroll -->
</div><!-- /.fullscreen-wrapper -->


@endsection
