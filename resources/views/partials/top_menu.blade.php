<div class="header-content">
    <div class="header-bottom">

        <ul class="header-nav-primary nav nav-pills collapse navbar-collapse">
            <li class="active">
                <a href="{{url('/home')}}">Home </a>
            </li>

            <li >
                @php
                if (Auth::check()) {
                @endphp
                    {{Form::open(['url'=>'logout','method'=>'post', 'class'=>'logout'])}}
                    <button type="submit">Logout</button>
                    {{Form::close()}}
                @php
                }else{
                @endphp
                    <a href="{{url('/login')}}">Login </a>
                @php
                }
                @endphp
            </li>
        </ul>

    </div><!-- /.header-bottom -->
</div><!-- /.header-content -->