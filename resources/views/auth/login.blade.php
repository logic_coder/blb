@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="page-title">
                <h1>Login</h1>
            </div><!-- /.page-title -->

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </ul>
                </div>
            @endif
            {{ Form::open(['url'=>'login','id'=>"form-login", "name"=>"signin-form","class"=>"p-t-15","role"=>"form","method"=>"post"]) }}
                <div class="form-group">
                    <label for="login-form-email">E-mail</label>
                    {{ Form::email('email', null ,['class' => 'form-control validate instant-validate','placeholder'=>'Email','required'=>'required']) }}
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label for="login-form-password">Password</label>
                    {{ Form::password('password' ,['class' => 'form-control','placeholder'=>'Password','required'=>'required']) }}
                </div><!-- /.form-group -->

                <button type="submit" class="btn btn-primary pull-right">Login</button>
            {{ Form::close() }}
        </div><!-- /.col-sm-4 -->
    </div><!-- /.row -->
@endsection
