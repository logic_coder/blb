@extends('layouts.master')

@section('content')
    <div class="mt-150">
        <div class="hero-image">
            <div class="hero-image-inner" style="background-image: url('images/tmp/slider-large-3.jpg');">
                <div class="hero-image-content">
                    <div class="container">
                    </div><!-- /.container -->
                </div><!-- /.hero-image-content -->

                <div class="hero-image-form-wrapper">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-6"></div>

                            <!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.hero-image-form-wrapper -->
            </div><!-- /.hero-image-inner -->
        </div><!-- /.hero-image -->

    </div>

    <div class="page-header">

        <div id="itemCards" class="item-cards cf clearfix">
        </div>
        <div id="itemCardsPlaceholder" class="item-cards-placeholder"></div>

        {{--<form method="post" action="#" id="plan-trip">--}}

            <input type="hidden" name="profile" value="">
            <input type="hidden" name="lodging-type" value="hotel">
            <input type="hidden" name="cities" value="102">

            <input type="hidden" name="preference-must_see" value="3">
            <input type="hidden" name="preference-culture" value="3">
            <input type="hidden" name="preference-cuisine" value="3">
            <input type="hidden" name="preference-adventure" value="3">
            <input type="hidden" name="preference-art" value="3">
            <input type="hidden" name="preference-f_and_e" value="3">
            <input type="hidden" name="preference-history" value="3">
            <input type="hidden" name="preference-museum" value="3">
            <input type="hidden" name="preference-nature" value="3">
            <input type="hidden" name="preference-r_and_r" value="3">
            <input type="hidden" name="preference-shopping" value="3">
            <input type="hidden" name="preference-sports" value="3">
            <input type="hidden" name="preference-budget" value="5">
            <input type="hidden" name="preference-pace" value="7">

            <div class="row row-flush">
                {{ Form::open(['url'=>'searchResult','id'=>"form-search", "name"=>"form-search","class"=>"p-t-15","role"=>"form","method"=>"post"]) }}
                <div class="col-xs-12 col-sm-12 col-md-6 left" id="sliders-step">
                    <h3 class="title text-center hidden">Personalize your trip</h3>
                    <div class="row city-select-wrapper ">
                        <div class="col-xs-12">
                            <div class="city">
                                <div class="form-group">
                                    <label for="place">Enter Distination</label>
                                    {{Form::select('location_id', $locations, null, ['class' => 'cs-select cs-skin-slide cs-transparent form-control','data-init-plugin' => 'cs-select']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group col-md-4">
                                {{ Form::label('start_date', 'Start Date') }}
                                {{ Form::date('start_date', null, ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group col-md-4">
                                {{ Form::label('end_date', 'End Date') }}
                                {{ Form::date('end_date', null, ['class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group col-md-4">
                                {{ Form::label('no_of_person', 'Number of person') }}
                                {{ Form::text('no_of_person', null, ['class' => 'form-control', 'required']) }}
                            </div>
                        </div>
                    </div>

                    <div class="row plan-step traveler-profiles" id="traveler-profiles">

                    </div>

                    <div class="preferences plan-step" id="preferences">
                        <h3 class="title text-center">Adjust your preferences:</h3>
                        <div class="row">
                            <div class="col-xs-6">
                                <h4>Budget</h4>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="slider budget-slider" data-preference="budget" data-value="5"></div>
                                    </div>
                                </div>

                                <div class="slider-values row">
                                    <select name="budget">
                                        <option value="5000">Backpacker</option>
                                        <option value="10000">Luxury</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <h4>Pace</h4>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="slider pace-slider" data-preference="pace" data-value="7"></div>
                                    </div>
                                </div>

                                <div class="slider-values row">
                                    <select name="pace">
                                        <option value="7">Relaxed</option>
                                        <option value="10">Pack it in!</option>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <hr>
                        <div class="tags">
                            <h3 class="title text-center tags-header">Refine your search</h3>
                            {{--<span class="tag unselected" data-state="unselected" data-id="Live Music">Live Music</span>--}}
                            {{--<span class="tag unselected" data-state="unselected" data-id="Family Friendly">Family Friendly</span>--}}
                            {{--<span class="tag unselected" data-state="unselected" data-id="Vegetarian,Vegan">Vegetarian</span>--}}
                            {{--<span class="tag unselected" data-state="unselected" data-id="Guided Tours,Specialty Tours">Tours</span>--}}
                            {{--<span class="tag unselected" data-state="unselected" data-id="Cheap Eats">Cheap Eats</span>--}}
                            {{--<span class="tag unselected" data-state="unselected" data-id="Foodie,Modern Cuisine,Local Cuisine">Foodie</span>--}}
                            {{--<span class="tag unselected" data-state="unselected" data-id="Romantic">Romantic</span>--}}
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <hr>
                                <h4>Your Traveler Profile</h4>
                                <br>
                            </div>
                        </div>

                        <div class="row row-flush">

                            <div class="col-xs-6 col-sm-3 col-md-4 text-center preference preference-must_see" style="color:#dec300">
                                <h4><i class="icon-must_see"></i>
                                    Shopping
                                    <span> <input type="checkbox" name="travel_profile" value="shopping"/></span>
                                </h4>
                            </div>

                            <div class="col-xs-6 col-sm-3 col-md-4 text-center preference preference-must_see" style="color:#37b0ab">
                                <h4><i class="icon-must_see"></i>
                                    Culture
                                    <span> <input type="checkbox" name="travel_profile" value="culture"/></span>
                                </h4>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-4 text-center preference preference-must_see" style="color:#2c407b">
                                <h4><i class="icon-must_see"></i>
                                    Cuisine
                                    <span> <input type="checkbox" name="travel_profile" value="cuisine"/></span>
                                </h4>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-4 text-center preference preference-must_see" style="color:#a0545e">
                                <h4><i class="icon-must_see"></i>
                                    Adventure
                                    <span> <input type="checkbox" name="travel_profile" value="adventure"/></span>
                                </h4>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-4 text-center preference preference-must_see" style="color:#71d1cf">
                                <h4><i class="icon-must_see"></i>
                                    Art
                                    <span> <input type="checkbox" name="travel_profile" value="art"/></span>
                                </h4>
                            </div><div class="col-xs-6 col-sm-3 col-md-4 text-center preference preference-must_see" style="color:#ed2d51">
                                <h4><i class="icon-must_see"></i>
                                    Entertainment
                                    <span> <input type="checkbox" name="travel_profile" value="entertainment"/></span>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Search</button>
                </div>
                {{ Form::close() }}

                <!-- // Map -->
                <div class="col-xs-12 col-sm-12 col-md-6 right plan-step" id="dates-step">
                    <div id="city-map" class="personalize-city-map" style="min-height:400px;"></div>
                    <div class="plan-your-trip" id="dates-box">

                    </div>
                    <!-- // plan-your-trip -->
                </div>
            </div>
        {{--</form>--}}
    </div>

    <script>
        var $Config = {};
    </script>

@endsection
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;client=gme-utripinc"></script>

<script src="{!! asset('assets/v2/js/build/vendor.min2e8e.js?rel=1493769838734') !!}"></script>

<script src="{!! asset('assets/v2/vendor/richmarker/richmarker.js') !!}"></script>

<script src="{!! asset('assets/v2/js/build/utrip.min2e8e.js?rel=1493769838734') !!}"></script>

<script type="text/javascript" src="http://api.tiles.mapbox.com/mapbox.js/v2.2.2/mapbox.js"></script>
<script type="text/javascript" src="{!! asset('assets/v2/vendor/leaflet/leaflet-heat.js') !!}"></script>
<script type="text/javascript">
    /*<![CDATA[*/
    L.mapbox.accessToken = 'pk.eyJ1IjoidXRyaXAiLCJhIjoiY2lmNGRwcDA1MDI3MnJya2xmc213d2YxdSJ9.w2aXXRBoOuYXuBF1tCJImQ';
    /*]]>*/
</script>